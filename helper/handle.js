const now = Date.now();
class Micro {
    async create(Pet) { //增加数据
        const dog = await Pet.create({
            id: 'd-' + now,
            name: 'Odie',
            gender: false,
            birth: '2008-08-08',
            createdAt: now,
            updatedAt: now,
            version: 0
        });
        console.log('created: ' + JSON.stringify(dog));
    }
    async deletDb() {
        const p = await queryFromSomewhere();
        await p.destroy();
    }
    async queryDB(Pet) {
        const pets = await Pet.findAll({
            where: {
                name: 'Odie'
            }
        });
        console.log(`find ${pets.length} pets:`);
        for (let p of pets) {
            console.log(JSON.stringify(p));
        }
    }
    async updateDB() {
        const p = await queryFromSomewhere();
        p.gender = true;
        p.updatedAt = Date.now();
        p.version++;
        await p.save();
    }
}

export default Micro