import Koa from 'koa'
import config from './db/config'
import views from 'koa-views'
import json from 'koa-json'
import session from 'koa-generic-session'
import Redis from 'koa-redis'
import onerror from 'koa-onerror'
import bodyparser from 'koa-bodyparser'
import logger from 'koa-logger'
import pv from './middleware/koa-pv'
import Pet from './model/pet'
import Micro from './helper/handle'


import index from './routes/index'
import users from './routes/users'

const app = new Koa()
const mc = new Micro()

// error handler
onerror(app)

app.keys = ['mt', 'keyskeys']
app.proxy = true
app.use(session(
    {
        key: 'mt', 
        prefix: 'mt:uuid', 
        store: new Redis(
            {
                host: config.host, 
                password: config.redisPsswd
            }
        ) 
    }
))


// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}))

// app.use(pv())
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))

app.use(views(__dirname + '/views', {
    extension: 'ejs'
}))

// logger
app.use(async(ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

// routes
app.use(index.routes()).use(index.allowedMethods())
app.use(users.routes()).use(users.allowedMethods())

// mc.create(Pet)
const db = mc.queryDB(Pet)
console.log(db)

// error-handling
app.on('error', (err, ctx) => {
    console.error('server error', err, ctx)
});

export default app