function pv(ctx) {
    ctx.session.count++
    console.log('pv', ctx.path)
}

export default function() {
    return async(ctx, next) => {
        pv(ctx)
        next()
    }
}