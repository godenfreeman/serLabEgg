import Router from 'koa-router'
import Redis from 'koa-redis'
import config from '../db/config'

let router = new Router({prefix: '/user'})

const Store = new Redis({host: config.host, password: config.redisPsswd}).client

router.get('/fox', async (ctx, next) => {
  const st = await Store.hset('fox', 'name', Math.random())
  console.log(`st: ${st}`) 
  ctx.body = {
    code:0,
    messgae:'ok'
  }
})

router.get('/bar', function (ctx, next) {
  ctx.body = 'this is a users/bar response'
})

export default router
