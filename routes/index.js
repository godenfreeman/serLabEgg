import Router from 'koa-router'

let router = new Router()

router.get('/', async(ctx, next) => {
    ctx.cookies.set('pvid', Math.random())
    await ctx.render('index', {
        title: 'Hello Koa 2111133333333!'
    })
})

router.get('/string', async(ctx, next) => {
    ctx.body = 'koa2 string'
})

router.get('/json', async(ctx, next) => {
    ctx.body = {
        title: 'koa2 json',
        cookie: ctx.cookies.get('pvid')
    }
})

router.get('/testAsync', async(ctx, next) => {
    console.log('start', new Date().getTime())
    const a = await new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('async a', new Date().getTime())
            resolve('a')
        }, 1000)
    })
    const b = await 12
    ctx.body = {
        a,
        b
    }
})

export default router