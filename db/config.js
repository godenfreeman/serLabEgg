const config = {
    database: 'test', // 使用哪个数据库
    username: 'root', // 用户名
    password: 'root', // 口令
    host: '47.105.163.213', // 主机名
    port: 3306, // 端口号，MySQL默认3306
    redisPsswd:'foobared',
    redisHost:'192.168.91.128',
    serPort:'3000'
};

// module.exports = config;
export default config